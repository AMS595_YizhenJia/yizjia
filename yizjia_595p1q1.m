% Project 1 Part 1
tic % Start Timing
N=100000; % Amount of ramdom points
num=0; % Store the points lies inside the quarter circle
for i=1:1:N
    x=rand(1,1);
    y=rand(1,1); % Create every ramdom point lies inside a 1x1 square
    if x^2+y^2<=1
        num=num+1;
    end
end
pi=4*num/N;
toc % End Timing

num=0; % Clear the stroage
ap=zeros(1,N); % Store every approximation of pi
for i=1:1:N
    x=rand(1,1);
    y=rand(1,1);
    if x^2+y^2<=1
        num=num+1;
    end
    ap(1,i)=num/i*4;
end

num=0; % Store the points lies inside the quarter circle
tic;
ct=zeros(1,N); % Store calculation time of approx pi
ap=zeros(1,N); % Store every approximation of pi
for i=1:1:N
    x=rand(1,1);
    y=rand(1,1);
    if x^2+y^2<1
     num=num+1;
    end
    ap(1,i)=num/i*4;
    ct(1,i)=toc;
end

plot(1:N,ap,'g',1:N,ones(1,N)*pi,'r') % Plot the approx pi in green and the true pi in red
axis([0 N 0 4]) % Set y axis approx pi value from 0 to 4
xlabel('num','FontSize',15)
ylabel('value','FontSize',15)
legend('simulation value','true value') % Explain two lines to readers
title('Amount of random numbers vs Approxiamtion of pi value','FontSize',15)

figure(2)
plot(ct,abs(pi-ap),'g') % Plot the total cost time vs the difference from approx pi to the true pi (precision)
axis([0 ct(i) -4 4])
xlabel('cost time','FontSize',15)
ylabel('precison','FontSize',15)
title('The total cost time vs the precision of approximated pi value','FontSize',15)

% User Guide
% This part of program uses Monte Carlo Algorithm to calculate the approximation of pi value. 
% Let a fixed number of random points orderly dropped in a 1*1 square area, inside this square, there is a  quarter circle with radius 1.
% 4*numbers of point dropped in the quarter circle / amount of dropped points considered as approximation of pi value.